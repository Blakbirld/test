﻿Shader "Custom/FlagShder"
{
    Properties{
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _Speed("Speed", Range(0, 5.0)) = 1
        _Frequency("Frequency", Range(0, 1.3)) = 1
        _Amplitude("Amplitude", Range(0, 5.0)) = 1
        _ScrollXSpeed("ScrollXSpeed", Range(0,10)) = 2
    }
        SubShader{
            Tags { "RenderType" = "Opaque" }
            Cull off

            Pass {

                CGPROGRAM
                #pragma vertex vert 
                #pragma fragment frag
                #include "UnityCG.cginc"

                sampler2D _MainTex;
                float4 _MainTex_ST;

                struct v2f {
                    float4 pos : SV_POSITION;
                    float2 uv : TEXCOORD0;
                };
                float _ScrollXSpeed;
                float _Speed;
                float _Frequency;
                float _Amplitude;
                bool _NeedScroll;

                v2f vert(appdata_base v)
                {
                    v2f o;
                    v.vertex.y += cos((v.vertex.x + _Time.y * _Speed) * _Frequency) * _Amplitude * (v.vertex.x - 5);
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    fixed xScroll = frac(_ScrollXSpeed * _Time);
                    fixed2 scrolledUV = i.uv;

                    scrolledUV += fixed2(xScroll, 0);

                    return tex2D(_MainTex, scrolledUV);
                }

                ENDCG

            }
        }
            FallBack "Diffuse"
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class Flag : FlagBase
{

	[SerializeField] private float windForce = 1;
	private float textureScrollSpeed = 0.5f;

    protected override void Start()
    {
        base.Start();

		StartCoroutine(FlagAnimation());
	}

    void Update()
	{
		float offset = Time.time * textureScrollSpeed;
		rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
	}

	private IEnumerator FlagAnimation()
	{
		float value = windForce;
		bool reverse = false;
		while (true)
		{
			yield return new WaitForSeconds(0.1f);
			
			for (int i = 0; i < vertices.Length; i++)
			{
				vertices[i].z = Mathf.PerlinNoise(vertices[i].x * value, vertices[i].y * value);
			}

			value += reverse?0.01f : -0.01f;

			if (value > windForce || value < -windForce)
				reverse = !reverse;

			mesh.vertices = vertices;
			mesh.RecalculateNormals();
		}
	}

}

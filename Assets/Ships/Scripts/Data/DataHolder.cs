﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHolder :MonoBehaviour
{
    public static DataHolder instance;

	public ShipData Ship1;
	public ShipData Ship2;

	public List<WeaponModule> WeaponModules;
	public List<AModule> Modules;

    #region Unity Metods
    private void Awake()
	{
		if (instance == this)
			DestroyImmediate(gameObject);
		else if (instance == null) 
			instance = this; 

		DontDestroyOnLoad(gameObject);

		LoadData();
	}
    #endregion

    #region DB Init
    //HardCode DB
    public void LoadData()
    {
		Ship1 = new ShipData() {
			Name = "Ship A",
			Health = 100,
			Shield = 80,
			ShieldRegeneration = 1,
			ShieldRegenerationTime = 1,
			ModulesCount = 2,
			WeaponsCount = 2
		};

		Ship2 = new ShipData()
		{
			Name = "Ship B",
			Health = 60,
			Shield = 120,
			ShieldRegeneration = 1,
			ShieldRegenerationTime = 1,
			ModulesCount = 3,
			WeaponsCount = 2
		};
		LoadModules();
		LoadWeapons();
	}

	private void LoadModules()
    {
		Modules = new List<AModule>();
		Modules.Add(new ModuleShield {ID = 0, ModuleName = "Module A", ShieldBoost = 50 });
		Modules.Add(new ModuleShield { ID = 1, ModuleName = "Module C", ShieldRegenerationBoost = 0.2f});
		Modules.Add(new ModuleHp { ID = 2, ModuleName = "Module B", HPBoost = 50});
		Modules.Add(new ModuleReload { ID = 3, ModuleName = "Module D", ReloadBoost = 0.2f});
	}

	private void LoadWeapons()
    {
		WeaponModules = new List<WeaponModule>();
		WeaponModules.Add(new WeaponModule() { ID = 4, ModuleName = "Gun A", AtackSpeed = 3, Damage = 5 });
		WeaponModules.Add(new WeaponModule() { ID = 5, ModuleName = "Gun B", AtackSpeed = 2, Damage = 4 });
		WeaponModules.Add(new WeaponModule() { ID = 6, ModuleName = "Gun C", AtackSpeed = 5, Damage = 20 });
	}

    #endregion

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class ShipData
{
    public string Name;
    public int Health;
    public int Shield;
    public int ShieldRegeneration;
    public float ShieldRegenerationTime;
    public int WeaponsCount;
    public int ModulesCount;
    public WeaponModule [] WeaponsModules;
    public AModule [] OtherModules;


   public void Build(int[] weaponsIDs, int[] modulesIDs)
    {
        DisposeModuls();
        WeaponsModules = new WeaponModule[WeaponsCount];
        OtherModules = new AModule[ModulesCount];
        for (int i = 0; i < OtherModules.Length; i++)
        {
            OtherModules[i] = DataHolder.instance.Modules.First(x => x.ID == modulesIDs[i]);
            if(OtherModules[i] != null)
              OtherModules[i].Init(this);
        }
        for (int i = 0; i < WeaponsModules.Length; i++)
        {
            WeaponsModules[i] = DataHolder.instance.WeaponModules.First(x => x.ID == weaponsIDs[i]);
            if (WeaponsModules[i] != null)
                WeaponsModules[i].Init(this);
        }
    }

    public void DisposeModuls()
    {
        if (WeaponsModules != null)
        {
            foreach (var item in WeaponsModules)
            {
                item.Dispose();
            }
        }
        if (OtherModules != null)
        {
            foreach (var item in OtherModules)
            {
                item.Dispose();
            }
        }
    }

    public string GetStats()
    {
        string data = string.Format("hp: {0} \n shield: {1} \n shield regeneration: {2}", Health, Shield, ShieldRegeneration);
        return data;
    }

}

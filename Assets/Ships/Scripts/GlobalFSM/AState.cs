﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AState : MonoBehaviour
{
    public State State;
    protected StateManager stateManager;

    public virtual void Enter(StateManager stateManager)
    {
        this.stateManager = stateManager;
        gameObject.SetActive(true);
    }

    public virtual void Exit()
    {
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public AState[] States;
    private AState m_CurrentState;

    private void Start()
    {
        SwitchState(State.Menu);
    }

    public void SwitchState(State newState)
    { 
        AState state = FindState(newState);
        if (state == null)
            return;

        if(m_CurrentState != null)
        m_CurrentState.Exit();

        m_CurrentState = state;
        m_CurrentState.Enter(this);
    }

    private AState FindState(State state)
    {
        foreach (var item in States)
        {
            if (item.State == state)
                return item;
        }

        return null;
    }

}
public enum State
{
    Menu,
    GamePlay
}
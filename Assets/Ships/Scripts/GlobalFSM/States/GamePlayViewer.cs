﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayViewer : AState
{
    [SerializeField] private Button menuBatton = null;
    [SerializeField] private GameObject completePanel = null;
    [SerializeField] private ShipBehaviour shipPrefab = null;


    private ShipBehaviour shipA = null;
    private ShipBehaviour shipB = null;
    public override void Enter(StateManager stateManager)
    {
        completePanel.gameObject.SetActive(false);
        base.Enter(stateManager);
        menuBatton.onClick.AddListener(GoToMenu);
        CreateShips();
    }

    private void CreateShips()
    {
        shipA = Instantiate(shipPrefab);
        shipB = Instantiate(shipPrefab);
        shipB.transform.position = new Vector3(0, 0, 10);
        shipA.Init(DataHolder.instance.Ship1, shipB);
        shipB.Init(DataHolder.instance.Ship2, shipA);

        shipA.OnDestoy += onShipDestroyed;
        shipB.OnDestoy += onShipDestroyed;
    }

    private void onShipDestroyed()
    {
        shipA.OnDestoy -= onShipDestroyed;
        shipB.OnDestoy -= onShipDestroyed;
        completePanel.gameObject.SetActive(true);
    }

    public override void Exit()
    {
        DataHolder.instance.Ship1.DisposeModuls();
        DataHolder.instance.Ship2.DisposeModuls();
        Destroy(shipA.gameObject);
        Destroy(shipB.gameObject);
        menuBatton.onClick.RemoveAllListeners();
        base.Exit();
    }

    private void GoToMenu()
    {
        stateManager.SwitchState(State.Menu);
    }
}

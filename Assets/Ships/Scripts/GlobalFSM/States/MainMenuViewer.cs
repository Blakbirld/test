﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuViewer : AState
{
    [SerializeField] UIShipViewer ship1 = null;
    [SerializeField] UIShipViewer ship2 = null;
    [SerializeField] Button playButton = null;


    private void Start()
    {
        ship1.Init(DataHolder.instance.Ship1);
        ship2.Init(DataHolder.instance.Ship2);

        playButton.onClick.AddListener(PlayClick);
    }

    private void PlayClick()
    {
        stateManager.SwitchState( State.GamePlay);
    }
}

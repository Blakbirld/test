﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWeaponView : UIModuleView
{

    protected override void ClickNext()
    {
        activeModule++;
        if (activeModule >= DataHolder.instance.WeaponModules.Count)
            activeModule = 0;

        SetupModule();
    }

    protected override void ClickPreview()
    {
        activeModule--;
        if (activeModule < 0)
            activeModule = DataHolder.instance.WeaponModules.Count - 1;

        SetupModule();
    }

    protected override void SetupModule()
    {
        var module = DataHolder.instance.WeaponModules[activeModule];
        CurrModuleID = module.ID;
        moduleName.text = module.ModuleName;
        OnChanged?.Invoke();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class UIShipViewer : MonoBehaviour
{
    public ShipData ShipData;

    [SerializeField] private TextMeshProUGUI shipName = null;
    [SerializeField] private TextMeshProUGUI shipStats = null;
    [SerializeField] private UIModuleView modulePrefab = null;
    [SerializeField] private UIModuleView WeaponPrefab = null;
    [SerializeField] private Transform modulesParent = null;

    private UIModuleView[] modules;
    private UIModuleView[] weapons;


    public void Init(ShipData ship)
    {
        ShipData = ship;
        shipName.text = ShipData.Name;
        modules = new UIModuleView[ShipData.ModulesCount];
        weapons = new UIModuleView[ShipData.WeaponsCount];

        for (int i = 0; i < modules.Length; i++)
        {
            UIModuleView view = Instantiate(modulePrefab, modulesParent);
            view.Init();
            view.OnChanged += onBuildShip;
            modules[i] = view;

        }

        for (int i = 0; i < weapons.Length; i++)
        {
            UIModuleView view = Instantiate(WeaponPrefab, modulesParent);
            view.Init(); 
            view.OnChanged += onBuildShip;
            weapons[i] = view;
        }
        onBuildShip();
    }

    private void OnDestroy()
    {
        foreach (var item in weapons)
        {
            item.OnChanged -= onBuildShip;
        }
        foreach (var item in modules)
        {
            item.OnChanged -= onBuildShip;
        }
    }

    private void onBuildShip()
    {
        int[] weap = weapons.Select(x => x.CurrModuleID).ToArray();
        int[] mod = modules.Select(x => x.CurrModuleID).ToArray();

        ShipData.Build(weap, mod);
        shipStats.text = ShipData.GetStats();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIModuleView : MonoBehaviour
{
    public Action OnChanged;
    public int CurrModuleID;

    [SerializeField] protected Button next, preview;
    [SerializeField] protected TextMeshProUGUI moduleName;
    protected int activeModule = 0;

    public void Init()
    {
        next.onClick.AddListener(ClickNext);
        preview.onClick.AddListener(ClickPreview);

        ClickNext();
    }

    protected virtual void ClickPreview()
    {
        activeModule--;
        if (activeModule < 0)
            activeModule = DataHolder.instance.Modules.Count - 1;

        SetupModule();
    }

    protected virtual void ClickNext()
    {
        activeModule++;
        if (activeModule >= DataHolder.instance.Modules.Count)
            activeModule = 0;

        SetupModule();
    }

    protected virtual void SetupModule()
    {
        var module = DataHolder.instance.Modules[activeModule];
        CurrModuleID = module.ID;
        moduleName.text = module.ModuleName;
        OnChanged?.Invoke();
    }

    protected void OnDestroy()
    {
        next.onClick.RemoveAllListeners();
        preview.onClick.RemoveAllListeners();
    }
}

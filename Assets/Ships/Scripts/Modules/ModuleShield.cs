﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleShield : AModule
{
    public int ShieldBoost;
    public float ShieldRegenerationBoost;

    private float modificator;
    public override void Init(ShipData shipData)
    {
        base.Init(shipData);
        ShipData.Shield += ShieldBoost;
        modificator = ShipData.ShieldRegenerationTime * ShieldRegenerationBoost;
        ShipData.ShieldRegenerationTime -= modificator;
    }

    public override void Dispose()
    {
        base.Dispose();
        ShipData.Shield -= ShieldBoost;
        ShipData.ShieldRegenerationTime += modificator;
    }

}

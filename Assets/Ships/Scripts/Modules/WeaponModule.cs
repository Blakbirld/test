﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class WeaponModule : AModule
{
    public float AtackSpeed;
    public int Damage;

    private float waitingTimer;
   
    public bool Fire()
    {
        waitingTimer += Time.deltaTime;
        if (waitingTimer > AtackSpeed)
        {         
            waitingTimer = 0;
            return true;
        }
        return false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AModule
{
    public ShipData ShipData;
    public string ModuleName;
    public int ID;

    public virtual void Init(ShipData shipData)
    {
        this.ShipData = shipData;
    }
    
    public virtual void Dispose()
    {

    }

    public void Tick()
    {

    }

    public virtual void Invoke()
    {
    
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleHp : AModule
{
    public int HPBoost;

    public override void Init(ShipData shipData)
    {
        base.Init(shipData);
        ShipData.Health += HPBoost;
    }

    public override void Dispose()
    {
        base.Dispose();
        ShipData.Health -= HPBoost;
    }

}

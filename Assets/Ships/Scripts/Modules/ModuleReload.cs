﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleReload : AModule
{
    public float ReloadBoost;

    public override void Invoke()
    {
        foreach (var item in ShipData.WeaponsModules)
        {
            item.AtackSpeed -= item.AtackSpeed * ReloadBoost;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehaviour : MonoBehaviour, IDamageable
{
    public Action OnDestoy;
    public ShipData shipData;

    [SerializeField] FlyingText shootText = null; 
    [SerializeField] FlyingText damageText = null;
    

    private ShipBehaviour target;
    private int currHealth;
    private int currShield;

    private bool isDeath = false;
    public void Init(ShipData data, ShipBehaviour targetShip)
    {
        currHealth = data.Health;
        currShield = data.Shield;
        shipData = data;
        target = targetShip;
        foreach (var item in shipData.OtherModules)
        {
            item.Invoke();
        }


        StartCoroutine(ShieldRegeneration());
    }

    public void AddDamage(int damage)
    {
        if (isDeath)
            return;

        damageText.Init(damage.ToString());
        if (currShield > 0)
            ShieldDamage(damage);
        else
            HealthDamage(damage);

    }

    private void ShieldDamage(int damage)
    {
        currShield -= damage;
        if (currShield < 0)
        {    
            HealthDamage(currShield * -1);
            currShield = 0;
        }
    }

    private void HealthDamage(int damage)
    {
        currHealth -= damage;
        if (currHealth <= 0)
        {
            Death();
            OnDestoy?.Invoke(); 
        }
    }

    private void Death()
    {
        isDeath = true;
    }

    private void Update()
    {
        if (isDeath )
            return;

        if (!target.isDeath)
        {
            foreach (var item in shipData.WeaponsModules)
            {
                if (item.Fire())
                {
                    shootText.Init("Shoot");
                    target.AddDamage(item.Damage);
                }
            }
        }
        foreach (var item in shipData.OtherModules)
        {
            item.Tick();
        }
    }

    private IEnumerator ShieldRegeneration()
    {
        while (gameObject.activeInHierarchy && isDeath == false)
        {
            yield return new WaitForSeconds(shipData.ShieldRegenerationTime);
            if (currShield < shipData.Shield)
                currShield += shipData.ShieldRegeneration;

            if (currShield > shipData.Shield)
                currShield = shipData.Shield;

        }
    }


}

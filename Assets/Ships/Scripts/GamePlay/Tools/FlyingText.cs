﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FlyingText : MonoBehaviour
{
    [SerializeField] private TextMeshPro textField = null;

    private float timeToDeactivate = 0.45f;
    public void Init(string text)
    {
        StopAllCoroutines();
        gameObject.SetActive(true);
        this.textField.text = text;
        transform.localPosition = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));
        StartCoroutine(Deactivate());
    }

    private IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(timeToDeactivate);
        gameObject.SetActive(false);
    }

}

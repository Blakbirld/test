﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class MetroControler : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI  patchText;
    [SerializeField] private Button calculateButton;
    [SerializeField] private TMP_InputField startField;
    [SerializeField] private TMP_InputField finishField;

    private Graph metro;
    private void Start()
    {
        CalculateMetroStations();
        calculateButton.onClick.AddListener(CalculateRoute);


    }

    private void CalculateMetroStations()
    {
        metro = new Graph();

        metro.AddVertex("A");
        metro.AddVertex("B");
        metro.AddVertex("C");
        metro.AddVertex("D");
        metro.AddVertex("E");
        metro.AddVertex("F");

        metro.AddVertex("H");
        metro.AddVertex("J");
        metro.AddVertex("O");
        metro.AddVertex("G");
        metro.AddVertex("K");
        metro.AddVertex("L");
        metro.AddVertex("M");
        metro.AddVertex("N");

        metro.AddEdge("A", "B");
        metro.AddEdge("B", "C");
        metro.AddEdge("C", "D");
        metro.AddEdge("D", "E");
        metro.AddEdge("E", "F");
        metro.AddEdge("B", "H");
        metro.AddEdge("H", "J");
        metro.AddEdge("D", "J");
        metro.AddEdge("C", "J");
        metro.AddEdge("J", "E");
        metro.AddEdge("J", "O");
        metro.AddEdge("J", "F");
        metro.AddEdge("F", "G");
        metro.AddEdge("K", "C");
        metro.AddEdge("K", "L");
        metro.AddEdge("L", "D");
        metro.AddEdge("L", "M");
        metro.AddEdge("M", "E");
        metro.AddEdge("L", "N");
    }

    private void CalculateRoute()
    {
        var dijkstra = new Finder(metro);
        var path = dijkstra.FindShortestPath(startField.text, finishField.text);
        patchText.text = path;
    }


}
